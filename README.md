Configure as variáveis de ambiente para `api/data/index.js` ou substitua os valores em:

````js
const config = {
    "user": process.env.MONGO_USER || 'user',
    "password": process.env.MONGO_PASS || 'password',
    "db": process.env.MONGO_DB || "pokemon",
    "server": process.env.MONGO_SERVER || "cluster0.6vpr4.mongodb.net"
}
````