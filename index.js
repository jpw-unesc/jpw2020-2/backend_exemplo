const express = require('express')
const cors = require('cors')
const app = express()
const route = {
    "pokemon": require('./api/routes/Pokemon')
}

app.listen(process.env.PORT || 8080)
app.use(express.json())
app.use(cors())

app.use('/pokemons', route.pokemon)