const express = require('express')
const router = express.Router()
var data = require('../models/Pokemon')

router.get('/', (req, res) => {
    data.find((err, doc) => {
        if(!err){
            res.json(doc)
        } else {
            res.json({
                "erro": "impossível retornar a lista"
            })
        }
    })
})

router.get('/:id', (req, res) => {
    var id = req.params.id
    data.findById(id, (err, doc) => {
        if(!err){
            res.json(doc)
        } else {
            res.json({
                "erro": "impossível retornar a objeto"
            })
        }
    })
})

router.post('/', (req, res) => {
    var pokemon = new data(req.body)
    pokemon.save().then((value) => {
        res.json(value)
    })
})

router.put('/:id', (req, res) => {
    var id = req.params.id
    var pokemon = req.body
    data.findByIdAndUpdate(id, pokemon, (err, doc) => {
        if(!err){
            res.json(doc)
        } else {
            res.json({
                "erro": "impossível atualizar o objeto"
            })
        }
    })
})

router.delete('/:id', (req, res) => {
    var id = req.params.id
    data.findByIdAndDelete(id, (err, doc) => {
        if(!err){
            res.json(doc)
        } else {
            res.json({
                "erro": "impossível deletar o objeto"
            })
        }
    })
})

module.exports = router