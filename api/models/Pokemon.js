const mongoose = require('../data')

var pokemonSchema = new mongoose.Schema({
    "numero": {
        type: Number,
        required: true
    },
    "nome": {
        type: String,
        required: true
    },
    "url": String
})

var pokemon = mongoose.model('pokemon', pokemonSchema)

module.exports = pokemon