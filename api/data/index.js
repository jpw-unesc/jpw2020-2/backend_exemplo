const mongoose = require('mongoose')
const config = {
    "user": process.env.MONGO_USER || 'user',
    "password": process.env.MONGO_PASS || 'password',
    "db": process.env.MONGO_DB || "pokemon",
    "server": process.env.MONGO_SERVER || "cluster0.6vpr4.mongodb.net"
}

mongoose.connect(`mongodb+srv://${config.user}:${config.password}@${config.server}/${config.db}?retryWrites=true&w=majority`, {useNewUrlParser: true, useUnifiedTopology: true})

mongoose.connection.on('connected', function(){
    console.log("MongoDB conectado")
})

mongoose.connection.on('error', function(err){
    console.log(err.message)
})

module.exports = mongoose